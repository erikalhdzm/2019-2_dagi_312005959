// Inicio.js Todo lo referente a inicio e inicio2 se encuentra declarado aqui
// Este documento hace referencia a ejemplosMatrices para pintar ejemplos 

var canvas = document.getElementById("iniIzq");

if (canvas) {
	
var ctx = canvas.getContext("2d");
ctx.font="8pt Arial";                     //Llenado del canvas iniIzq
ctx.fillStyle = "blue";
ctx.textAlign = "left"; 
ctx.fillText("Una matriz es una tabla rectangular de datos",10,30);
ctx.fillText("ordenados en filas y columna.",10,40);
ctx.fillText("Si una matriz tiene m filas y n columnas",10,60);
ctx.fillText(" decimos que la matriz es de orden mxn. ",10,70);
ctx.fillText("Todos los elementos de las matrices se denotan",10,90);
ctx.fillText(" con subindices aij, el valor de i representa la fila",10,100);
ctx.fillText(" y el valor de j la columna. Los valores de i van de",10,110);
ctx.fillText(" 1 a m y los valores de j van de 1 a n",10,120);

var canvas1 = document.getElementById("iniDer");   
var ctx1 = canvas1.getContext("2d");
var  dimension =Math.floor(Math.random()*(8+1));  //Variable random para la dimension

var ejemploInicio = parent.crearMatriz(dimension,dimension);

parent.dibujaMatriz(ejemploInicio, canvas1, ctx1);
var1 = Math.floor(Math.random() * dimension); 
var2 = Math.floor(Math.random() * dimension);  
document.getElementById('var1').innerHTML = var1;
document.getElementById('var2').innerHTML = var2;
function verificar(){   //Declaro la matriz afuera????????????
	resultado = document.getElementById('resultado').value;

	if (resultado == ejemploInicio[var1][var2]){
			alert("CORRECTO");
	}else{
			alert("NO ES CORRECTO");
	}
	var1 = Math.floor(Math.random() * dimension); 
	var2 = Math.floor(Math.random() * dimension);  
}                                           /***********************Hasta aqui pertenece a inicio********************/

}else{                            /*******************Lo siguiente pertenece a inicio2**********************/

var canvas = document.getElementById("letras");    //Letrero para inicio2
var ctx = canvas.getContext("2d");
ctx.font="8pt Arial";
ctx.textAlign = "left"; 
ctx.fillText("Existen varios tipos de matrices",10,10);
ctx.fillText("de acuerdo a su forma y a su",10,20);
ctx.fillText("contenido. Por ejemplo, la matriz",10,30);
ctx.fillText(" M = [3 2 1 2 4 1 ] se conoce ",10,40);
ctx.fillText("como matriz fila, por su forma.",10,50);
ctx.fillText("Otros tipos de matrices, las",10,70);
ctx.fillText("puedes reconocer facilmente,",10,80);
ctx.fillText("seleccionando alguna de las ",10,90);
ctx.fillText("opciones de la ventana izquierda.",10,100);
ctx.fillText("Cuando aparezca el boton de ",10,120);
ctx.fillText("ayuda, puedes hacer clic varias",10,130);
ctx.fillText("veces, para observar varias matrices.",10,140);
ctx.fillStyle = "black";

	
}


/*La funcion ejemplo nos dara ejemplos con metrices de diferentes tamanios .
*Como parametro es una cadena que dependediendo cual sea se contruira 
*la matriz con diferentes propiedades.
*Tambien se muestran diferentes mensajes de acuerdo a la cadena.
*/
function ejemplo(cad){ 
var canvas = document.getElementById('matrices');
var ctx = canvas.getContext("2d");
ctx.font="8pt Arial";
var dimension= Math.floor( (Math.random()*(8+1)) + 2);  //Variable para dimension
console.log(dimension);
var canvas1 = document.getElementById('mensajes');    
var ctx1 = canvas1.getContext("2d");
 ctx1.font="8pt Arial";
ctx1.clearRect ( 0 , 0 , canvas1.width , canvas1.height );
		ctx.clearRect ( 0 , 0 , canvas.width , canvas.height );
	switch(cad){
		case "simetrica":
		ctx.textAlign = "center"; 
		ctx1.fillText("Observa los terminos por encima de  la diagonal",0, canvas1.height/6 * 1 );
		ctx1.fillText("principal y comparalos con los terminos debajo de",0,canvas1.height/6  * 2 );
		ctx1.fillText("la diagonal principal",0,canvas1.height/6 * 3);
				  ctx1.fillText("Si quieres ver otro ejemplo preciona de nuevo el boton", 0, canvas1.height/6 * 4);
		var ejemploSimetrica = simetrica(dimension,dimension);
        parent.dibujaMatriz(ejemploSimetrica, canvas, ctx);
        break;
		
		case "superior":
		ctx.textAlign = "center"; 
         ctx1.fillText("Observa los terminos superiores", 0, canvas1.height/5 * 1);
		 ctx1.fillText("por debajo de la diagonal principal",0, canvas1.height/5 * 2);
		 		  ctx1.fillText("Si quieres ver otro ejemplo preciona de nuevo el boton", 0, canvas1.height/5 * 3);
		 var ejemploSuperior = superior(dimension,dimension);
        parent.dibujaMatriz(ejemploSuperior, canvas, ctx);
		break;
		
		case "inferior":
		ctx.textAlign = "center"; 
		 ctx1.fillText("Observa los terminos inferiores", 0, canvas1.height/5 * 1);
		 ctx1.fillText("por encima de la diagonal principal", 0, canvas1.height/5 * 2);
		 		  ctx1.fillText("Si quieres ver otro ejemplo preciona de nuevo el boton", 0, canvas1.height/5 * 3);
		 var ejemploInferior = inferior(dimension,dimension);
        parent.dibujaMatriz(ejemploInferior, canvas, ctx);
		break;
		
		case "nula":
         ctx.textAlign = "start"; 
		ctx1.fillText("¡Todo en ceros!", 0, canvas1.height/2);
		var ejemploNula = nula(dimension,dimension);
        parent.dibujaMatriz(ejemploNula, canvas, ctx);
		break;
		
		case "identidad":
		ctx.textAlign = "center"; 
		 ctx1.fillText("¡Facil!" , 0, canvas1.height/5 * 1);
		 ctx1.fillText("Unos en la diagonal principal y ", 0, canvas1.height/ 5* 2);
		  ctx1.fillText("ceros en el resto", 0, canvas1.height/5 * 3);
		  ctx1.fillText("Si quieres ver otro ejemplo preciona de nuevo el boton", 0, canvas1.height/5 * 4);
		  var ejemploIdentidad = identidad(dimension,dimension);
       
        parent.dibujaMatriz(ejemploIdentidad, canvas, ctx);
		break;
		
		case "diagonal":
		ctx.textAlign = "center"; 
		ctx1.fillText("¡Otra facil!", 0, canvas1.height/7 * 1);
		ctx1.fillText("¡Los elementos por fuera de la diagonal principal", 0, canvas1.height/7 * 2);
		ctx1.fillText("son todos iguales a cero, debe haber, al emnos,un", 0, canvas1.height/7 * 3);
		ctx1.fillText("elemento diferente de cero en la diagonal principal!", 0, canvas1.height/7 * 4);
		ctx1.fillText("Si quieres ver otro ejemplo preciona de nuevo el boton", 0, canvas1.height/7 * 5);
		var ejemploDiagonal = diagonal(dimension,dimension);
        parent.dibujaMatriz(ejemploDiagonal, canvas, ctx);
		break; 
   }
}

