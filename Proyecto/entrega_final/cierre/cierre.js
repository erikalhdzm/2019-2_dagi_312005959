var canvas = document.getElementById("descripcion");  /*Cierre2*/
var canvas1 = document.getElementById("matrizA");    /*Cierre3*/
var canvas2 = document.getElementById("matrizB");        /*Cierre3*/
var canvas3 = document.getElementById("suma");     /*Cierre3*/
var canvas4 = document.getElementById("descFinal");   /*Cierre4*/
var canvas5 = document.getElementById("matrizAC");    /*Cierre4*/
var canvas6 = document.getElementById("matrizBC");    /*Cierre4*/
var canvas7 = document.getElementById("operacion");    /*Cierre4*/

if (canvas) {      /*************************Cierre2*************/
	
     var ctx = canvas.getContext("2d");
     ctx.font="8pt Arial";
     ctx.fillStyle = "blue";
     ctx.fillText("En este ejercicio, practicaremos la suma de matrices a traves de colores",10,20);
     ctx.fillText("El proposito es recordar el procedimiento de la suma",10,30);
     ctx.fillText("La suma de A y B da como resultado otra matriz de colores",10,40);
     ctx.fillText(" Recuerda que dos colores iguales dan el mismo color.",10,50);
     ctx.fillText("Para colores diferentes, ten en cuenta:",10,70);
     ctx.fillText("Amarillo + Azul = Verde",10,80);
     ctx.fillText("Rojo + Amarillo = Naranja",10,90);
     ctx.fillText("Rojo + Blanco = Rosa",10,100);
     ctx.fillText("Azul + Blanco = Turquesa",10,110);
     ctx.fillText("Rojo + Azul = Magenta",10,120);
	 ctx.fillText("Blanco + Amarillo = Amarillo claro  :P",10,130);

}else if(canvas1){                  /*********************Cierre3*******************/
	 var ctx1 = canvas1.getContext("2d");
     var ctx2 = canvas2.getContext("2d");
	 var ctx3 = canvas3.getContext("2d");
	 var  dimension =Math.floor(Math.random()*(8+1)); 	 //Variable random para la dimension
     var  escalar =Math.floor(Math.random()*(6+1));
	 var matriz1 = parent.crearMatriz(dimension,dimension);
     var matriz2 = parent.crearMatriz(dimension,dimension);
	 var sumaFinal = parent.suma(matriz1,matriz2);
	 parent.dibujaMatriz(matriz1, canvas1, ctx1);
	 parent.dibujaMatriz(matriz2, canvas2, ctx2);
	 parent.dibujaMatriz(sumaFinal, canvas3, ctx3);
	
}else if(canvas4){          /****************Cierre4******************/
	
	 var ctx4 = canvas4.getContext("2d");
	 var ctx5 = canvas5.getContext("2d");
	 var ctx6 = canvas6.getContext("2d");
	 var ctx7 = canvas7	.getContext("2d");
     ctx4.font="12pt Arial";
     ctx4.fillStyle = "blue";
     ctx4.fillText("Este   es   el   ultimo    ejercicio",10,20);
     ctx4.fillText("de    matrices,   para   que ",10,30);
     ctx4.fillText("practiques la multiplicacion",10,40);
	 var  dimension =Math.floor(Math.random()*(8+1)); 	 //Variable random para la dimension
	var matriz1 = parent.crearMatriz(dimension,dimension);
    var matriz2 = parent.crearMatriz(dimension,dimension);
	parent.dibujaMatriz(matriz1, canvas5, ctx5);
	parent.dibujaMatriz(matriz2, canvas6, ctx6);	
	var ejemploMultiMat =parent.multiplicacion(matriz1,matriz2);
	 parent.dibujaMatriz(ejemploMultiMat, canvas7, ctx7);	
}

