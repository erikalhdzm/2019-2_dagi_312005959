var canvas = document.getElementById("desIzq");     /*Desarrollo 1*/
var canvas1 = document.getElementById("suma");      /*Desarrollo 1*/
var canvas6 = document.getElementById("matrizA");          /*Desarrollo 1*/
var canvas7 = document.getElementById("matrizB");           /*Desarrollo 1*/
var canvas2 = document.getElementById("ejemplo");         /*Desarrollo 2*/
var canvas3 = document.getElementById("ejemplosol");    /*Desarrollo 2*/
var canvas4 = document.getElementById("desExp");         /*Desarrollo 2*/
var canvas5 = document.getElementById("desExp2");       /*Desarrollo 2*/
var canvas8 = document.getElementById("suma3");      /*Desarrollo 3*/
var canvas9 = document.getElementById("matrizA3");          /*Desarrollo 3*/
var canvas10 = document.getElementById("matrizB3");           /*Desarrollo 3*/
var canvas11 = document.getElementById("explicacion");         /*Desarrollo 3*/



if (canvas) {                                                    /**********************Desarrollo.html****************************/
var ctx = canvas.getContext("2d");
ctx.font="8pt Arial";
ctx.fillStyle = "blue";
ctx.fillText("La suma de matrices tiene algunas propiedades:",10,20);
ctx.fillText("les conmutativa, asociativa y distributiva con respecto",10,30);
ctx.fillText("a la multiplicacion por un escalar, tiene modulo e inversa",10,40);
ctx.fillText(" A + B = B + A                                  Conmutativa",10,60);
ctx.fillText("A + (B + C) = (A + B) + C                Asociativa",10,70);
ctx.fillText("k (A + B) = kA +kB                          Distributiva",10,80);
ctx.fillText("A + 0= 0 + A = A                              Modulativa",10,90);
ctx.fillText("En la ventana izquierda, puedes observar como se",10,110);
ctx.fillText("calcula la suma de matrices.",10,120);
ctx.fillText("Inicia el calculo, haciendo clic en el boton Iniciar.",10,140);
 var ctx2 = canvas1.getContext("2d");
  var ctx6 = canvas6.getContext("2d");
   var ctx7 = canvas7.getContext("2d");
    var  dimension =Math.floor(Math.random()*(6+1));  //Variable random para la dimension
    var matriz1 = parent.crearMatriz(dimension,dimension);
    var matriz2 = parent.crearMatriz(dimension,dimension);
    var ejemploSuma = parent.suma(matriz1, matriz2);
    parent.dibujaMatriz(matriz1, canvas6, ctx6);
	parent.dibujaMatriz(matriz2, canvas7, ctx7);	
	parent.dibujaMatriz(ejemploSuma, canvas1, ctx2);	
}else if (canvas8){                                 /*************************Desarrollo 3 ******************************/
    var ctx8 = canvas8.getContext("2d");
    var ctx9 = canvas9.getContext("2d");
    var ctx10 = canvas10.getContext("2d");
    var ctx11 = canvas11.getContext("2d");
ctx11.font="8pt Arial";
ctx11.fillStyle = "yellow";
ctx11.fillText("PROPIEDADES DE LA MULTIPLICACION",10,20);
ctx11.fillStyle = "blue";
ctx11.fillText(" A * B != B * A                               No es conmutativa",10,40);
ctx11.fillText("A * (B * C) = (A * B) * C               Es asociativa",10,50);
ctx11.fillText("A * A-1 = A-1 * A = I                     Es inversa",10,60);
ctx11.fillText("A * I= I * A = A                               Es modulativa",10,70);
ctx11.fillStyle = "blue";
ctx11.fillText("En esta escena, comprenderas cuales son",10,90);
ctx11.fillText("los pasos a seguir para realizar la multiplicacion",10,100);
ctx11.fillText("entre dos matrices.",10,110);
ctx11.fillText("Haz click en el boton de Iniciar.",10,130);
	var  dimension =Math.floor(Math.random()*(6+1));  //Variable random para la dimension
    var matriz1 = parent.crearMatriz(dimension,dimension);
    var matriz2 = parent.crearMatriz(dimension,dimension);
	var ejemploMultiMat = parent.multiplicacion(matriz1, matriz2);
	parent.dibujaMatriz(matriz1, canvas9, ctx9);
	parent.dibujaMatriz(matriz2, canvas10, ctx10);	
	parent.dibujaMatriz(ejemploMultiMat, canvas8, ctx8);	

}else if (canvas2){                      /*****************Desarrollo2************************/
	var  dimension =Math.floor(Math.random()*(8+1));  //Variable random para la dimension
	var ctx = canvas2.getContext("2d");
	var ctx3 = canvas3.getContext("2d");
	var ctx4 = canvas4.getContext('2d');
    var ctx5 = canvas5.getContext('2d');
    var matriz = parent.crearMatriz(dimension,dimension);
	var escalar = Math.floor(Math.random()*(20+1));  
	 var ejemploMultEsc = parent.multEsc(matriz, escalar);
	 parent.dibujaMatriz(matriz, canvas2, ctx);
	parent.dibujaMatriz(ejemploMultEsc, canvas3, ctx3);	
	

 var explicacionEjemplo = [["a11","a12","a13","...","a1n"],["a21","a22","a23","...","a2n"]
                                        ,["a31","a32","a33","...","a3n"],[".",".",".","...","."], [".",".",".","...","."]
										,[".",".",".","...","."],["am1","am2","am3","...","amn"]]
										
var explicacionEjemplo1 = [["k*a11","k*a12","k*a13","...","k*a1n"],["k*a21","k*a22","k*a23","...","k*a2n"]
                                        ,["k*a31","k*a32","k*a33","...","k*a3n"],[".",".",".","...","."], [".",".",".","...","."]
										,[".",".",".","...","."],["k*am1","k*am2","k*am3","...","k*amn"]]
										
   
     parent.dibujaMatriz(explicacionEjemplo, canvas4, ctx4);		
parent.dibujaMatriz(explicacionEjemplo1, canvas5, ctx5);		
 
}

