export default class Model {
  constructor() { }

  init(controller) {
    this.canvas = document.getElementById("drawing");
    this.context = this.canvas.getContext("2d");

    this.controller = controller;
  }

  setImage(image) {
    this.context.drawImage(image, 0, 0);
    this.redo_step = 0;   //Deshacer 
    this.undo_step = 0;
    this.undo_list = [image];
  }

  getImage() {
    return this.canvas.toDataURL("image/png");
  }

  putImage(image) {
    this.context.drawImage(image, 0, 0);

    let img = document.createElement("img");
    img.src = this.canvas.toDataURL("image/png");
    this.undo_step++;  //Aumento de pasos
    this.redo_step = 0;
    this.undo_list[this.undo_step] = img;
  }

  undo() {
    if (this.undo_step > 0) {
      this.undo_step--;
      this.redo_step++;
      this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.context.drawImage(this.undo_list[this.undo_step], 0, 0);
    }
  }
  redo(){  //Funcion para el boton redo 

    if(this.redo_step  > 0 && (typeof this.undo_list[this.undo_step + 1] !== 'undefined')){   //Verifica que exista al menos una accion de deshacer

      this.undo_step++;
      this.redo_step++;
      this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.context.drawImage(this.undo_list[this.undo_step], 0, 0);
    }
  }
}