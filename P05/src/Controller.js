export default class Controller {
  constructor() { }

  init(model, view) {
    this.model = model;
    this.view = view;
  }

  open() {
    let file_input = document.createElement("input");
    file_input.setAttribute("type", "file");
    file_input.setAttribute("accept", "image/png,image/jpeg");

    file_input.addEventListener("change", (evt) => {
      let file = file_input.files[0];
      
      const reader = new FileReader();
      reader.addEventListener("load", (elem) => {
        let image = document.createElement("img");
        
        image.addEventListener("load", () => {
          this.view.setTitle(file.name);
          this.view.changeSize(image.width, image.height);
          this.model.setImage(image);
        });
        image.src = reader.result;
      });
      if (file) {
        reader.readAsDataURL(file);
      }
    });

    file_input.click();
  }

  save() {
    let link = document.createElement("a");
    link.setAttribute("download", "image.png");
    document.body.appendChild(link);
    
    link.addEventListener("click", (evt) => {
      link.href = this.model.getImage();
      document.body.removeChild(link);
    });

    link.click();
  }

  undo() {
    this.model.undo();
  }
  redo(){
    this.model.redo();  //Deshace el ultimo cambio
  }

  setStrokeColor(color) {
    this.strokeColor = color;
  }
  
  setCirculeColor(color){
	this.CirculeColor = color;
  } //Asigna color del circulo
    setRectColor(color){ //Asigna color del rectangulo
	this.RectColor = color;
  }
  line_mode() {
    this.mode = "line";
  }
  rect_mode(){//Crear rectas
    this.mode = "rect";
  }
  rect_fill_mode(){ //Dar color a la linea
    this.mode = "rect_fill";
  }
  circle_mode(){   //Dibujar ciruculos
    this.mode = "circle";
  }
  circle_fill_mode(){   //Dar relleno de color a los circulos
    this.mode = "circle_fill";
  }
  free_mode() {
    this.mode = "free";
  }

  mouse_down(evt, canvas) {
    if ((this.mode === "line") || (this.mode === "free")) {
      this.rect = canvas.getBoundingClientRect();
      this.init_x = evt.clientX - this.rect.left;
      this.init_y = evt.clientY - this.rect.top;

      this.view.setStrokeColor(this.strokeColor);
    }  //Asigna la posicion al rectangulo 
    if(this.mode === "rect" || this.mode === "rect_fill"){
      this.rect = canvas.getBoundingClientRect();
      this.init_x = evt.clientX - this.rect.left;
      this.init_y = evt.clientY - this.rect.top
      
	  this.view.setStrokeColor(this.strokeColor);
	  this.view.setRectColor(this.rectColor);
    }//Asigna la posicion a los circulos
    if(this.mode === "circle" || this.mode === "circle_fill"){
      this.rect = canvas.getBoundingClientRect();
      this.init_x = evt.clientX - this.rect.left;
      this.init_y = evt.clientY - this.rect.top;
	  
	  this.view.setStrokeColor(this.strokeColor);
    }
    if (this.mode === "free") {
      this.view.initFree(this.init_x, this.init_y);
    }
  }

  mouse_move(evt) {
    if (this.mode === "line") {
      this.view.drawLine(
        this.init_x, this.init_y, 
        evt.clientX - this.rect.left, evt.clientY - this.rect.top
      );
    }
    else if (this.mode === "free") {
      this.view.continueFree(evt.clientX - this.rect.left, evt.clientY - this.rect.top);
    }else if(this.mode === "circle" || this.mode === "circle_fill"){  //Dibuja circulos
      this.view.drawCircle(   
          this.init_x, this.init_y,
          evt.clientX - this.rect.left, evt.clientY - this.rect.top, (this.mode === "circle_fill")
      )
    }else if(this.mode === "rect" || this.mode === "rect_fill"){  //Dibuja rectangulos
      this.view.drawRect(
          this.init_x, this.init_y,
          evt.clientX - this.rect.left, evt.clientY - this.rect.top, (this.mode === "rect_fill")
      )
    }
  }

  mouse_up(aux_canvas) {
    this.model.putImage(aux_canvas);

    this.view.clear();
  }
}