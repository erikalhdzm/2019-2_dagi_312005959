window.onload = function () {
     var canvas = document.createElement('canvas');   //DeclaracionCanvas
     var context = canvas.getContext('2d');
     var score = 0;      // Variable puntaje
     var segSize = 10;
     var velocidad = 6;     //Velocidad a la que va la viborita
     var sentido = 'right';      //sentidoeccion por defecto
     var snake = new Array(4);   //Viborita por defecto
     var inPlay = true;   //Nos dice si aun seguimos jugando
     var tablero = new Array(300);    //Espacio de canvas 
     
	 
	 
	 for (i = 0; i < tablero.length; i++) {
       tablero[i] = new Array(300);    // Arreglo para el juego
     }
     console.log(tablero);
     canvas.width = 768;
     canvas.height = 576;
     
	 var body = document.getElementsByTagName('body')[0];
     body.appendChild(canvas);
     tablero = crearSnake(tablero);    //Lamada a crear la serpiente
     tablero = crearManzana(tablero);       // Llamada a crear la comida 
     juegoSnake();
	 
	 
	 
	 //Funcion donde se pinta en canvas y las letras para mostrar el score
     function paintCanvas() {
        context.strokeSytle = "black";
        context.strokeRect(0, 80, canvas.width, canvas.height - 80);
        context.font = 'bold 80px Arial';
        context.fillStyle = 'black';
		context.fillText("Score: " + score, (canvas.width/2)-context.measureText("Score: "+score).width/2, 60);
		
		context.lineWidth = 1;     //Se pintan las lineas horizontales
         for (let y = 80; y < 1800; y += 10) {
           context.moveTo(0, y);
           context.lineTo(1600, y);
         }
         context.stroke();
		 
		  for (let x = 0; x < 1800; x += 10) { /*Se pintaran las lineas divisoras verticales*/ 
           context.moveTo(x , 100);
           context.lineTo(x, 900);
         }
       context.stroke();
     }
	
    //Funcion para crear una manzana 	
     function crearManzana(tablero) {
         var manzanaX = Math.round(Math.random() * 30);
         var manzanaY = Math.round(Math.random() * 30);
         while (tablero[manzanaX][manzanaY] == 'S') {
            manzanaX = Math.round(Math.random() * 80);
            manzanaY = Math.round(Math.random() * 80);
         }
         tablero[manzanaX][manzanaY] = 'F';
         return tablero;
     }
	
    //Funcion que crea la viborita, siempre comienza en la misma posicion	
     function crearSnake(tablero) {
         var snakeX = 31;    
         var snakeY = 25;
         while ((snakeX - snake.length) < 0) {
         snakeX = Math.round(Math.random() * 80);
         }
         for (i = 0; i < snake.length; i++) {
            snake[i] = {x: snakeX - i, y: snakeY};
            tablero[snakeX - i][snakeY] = "S";
         } 
         return tablero;5
     }
	 
	 /*Funcion para mostrar al usuario su usuario y recordarle que le de refresh para vovler a jugar*/
     function finJuego() {
     alert("Juego terminao, el puntaje obtenido es : " + score + " Presione f5 para volver a intentar");
     }	
	 
	 
     /********* Comandos para cambio de direccion ****/
	 window.addEventListener('keydown', function (e) {
     if (e.keyCode == 37 && sentido != 'right') {
         sentido = 'left';
      } else if (e.keyCode == 38 && sentido != 'down') {
         sentido = 'up';
      } else if (e.keyCode == 39 && sentido != 'left') {
         sentido = 'right';
      } else if (e.keyCode == 40 && sentido != 'up') {
         sentido = 'down';
      }
     });
	 
	 
	 //Funcion Juego Principal
     function juegoSnake() {     
     context.clearRect(0, 0, canvas.width, canvas.height);
     for (i = snake.length - 1; i >= 0; i--) {
        if (i == 0) {
            switch (sentido) {
                case 'right':
                   if (sentido != 'left') {
                       snake[0] = { x: snake[0].x + 1, y: snake[0].y }
                break;
                }
                case 'left':
                   if (sentido != 'right') {
                       snake[0] = { x: snake[0].x - 1, y: snake[0].y }
                break;
                }
                case 'up':
                     if (sentido != 'down') {
                        snake[0] = { x: snake[0].x, y: snake[0].y - 1 }
                break;
                }
                case 'down':
                    if (sentido != 'up') {
                       snake[0] = { x: snake[0].x, y: snake[0].y + 1 }
                break;
                }
            }
			
			/*******Verificación de choque con las esquinas *******/
            if(snake[0].x < 0 || snake[0].x >=768 ||snake[0].y <0 || snake[0].y >=768) {
               finJuego();
               return;
            }
			
			/*******Verificación de cuando se come una manzana ******/
            if(tablero[snake[0].x][snake[0].y] == 'F') {
                score++;     //Sube el score
                tablero=crearManzana(tablero);   //Se vuelve a crear una manzana

            }else if (tablero[snake[0].x][snake[0].y] == 'S') {
                finJuego();
                return;
            }
            tablero[snake[0].x][snake[0].y] = 'S';
            } else {
                if(i==(snake.length-1)){
                    tablero[snake[i].x][snake[i].y]=null;
                }
                snake[i]={
                x: snake[i-1].x,
                y: snake[i-1].y
                };
                tablero[snake[i].x][snake[i].y] = 'S';
           }
     }
	 
     paintCanvas();
     for (x = 0; x < tablero.length; x++) {
         for (y = 0; y < tablero[0].length; y++) {
            if (tablero[x][y] == 'F') {
            context.fillStyle = 'red';
            context.fillRect(x * segSize, y * segSize + 80, segSize, segSize);
            }
            else if (tablero[x][y] == 'S') {
            context.fillStyle = 'blue';
            context.strokeSytle = 'lightgrey';
            context.fillRect(x * segSize, y * segSize + 80, segSize, segSize);
            context.strokeRect(x * segSize, y * segSize + 80, segSize, segSize);
            }
         }
     }
        if(inPlay){
           setTimeout(juegoSnake, 400-(velocidad*50));
        }
     }
	 
};