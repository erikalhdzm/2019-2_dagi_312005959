export default class Interactive {
  constructor(container) {
    this.container = container;
    this.block = document.getElementById("div_0");
//this.block = document.getElementById("cuadro");
    this.block.r = 0;
    this.block.g = 128;
    this.block.b = Math.random()*256;
	this.block.x = 32;
    this.block.y = 24;
    this.block.speed = 5;
	this.block.selec = 1503;
	
	
  }

  processInput() {
    
  }

  update(elapsed) {
	var arr_izq = [0,64,128,192,256,320,448,512,576,640,704,768,832,896,960,1024,1088,1152,1216,1280,1344,1408,1472,1536,1600,1664,1728,1792,1856,1920,1984,2048,2112,2176,2240,2304,2368,2432,2496,2560,2624,2688,2752,2816,2880,2944,3008];
	var arr_der = [63,127,191,255,319,447,511,575,639,703,767,831,895,959,1023,1087,1151,1215,1279,1343,1407,1471,1535,1599,1663,1727,1791,1855,1919,1983,2047,2111,2175,2239,2303,2367,2431,2495,2559,2623,2687,2751,2815,2879,2943,3007,3071];
	var arr_arriba = [];
	var arr_abajo = [];
	for(var x = 3008; x < 3072; x++){
		arr_abajo.push(x);
	}
	for(var x = 0; x < 63; x++){
		arr_arriba.push(x);
	}
	var bloque_anterior = this.block;
	
	this.block = document.getElementById("div_" + bloque_anterior.selec);
	this.block.mov = "hor";
	this.block.dir = "izq";
	this.block.selec = bloque_anterior.selec;

	if(this.block.mov == "vert"){
	
		if(this.block.dir == "izq"){
		
		if(!arr_arriba.includes(this.block.selec)){
		
			this.block.selec = (bloque_anterior.selec - 1) % 3072;
			var contenedor = document.getElementById("container");
			this.block.r = (this.block.r + this.block.speed * elapsed) % 256;
			this.block.g = (this.block.g + this.block.speed * elapsed) % 256;
			this.block.b = (this.block.b + this.block.speed * elapsed) % 256;
			
			this.block.style.backgroundColor = "red";//`rgb(${this.block.r},${this.block.g},${this.block.b})`;
		}
		}
		else{
			if(!arr_abajo.includes(this.block.selec)){
				
							
			this.block.selec = (bloque_anterior.selec + 1) % 3072;
			var contenedor = document.getElementById("container");
			this.block.r = (this.block.r + this.block.speed * elapsed) % 256;
			this.block.g = (this.block.g + this.block.speed * elapsed) % 256;
			this.block.b = (this.block.b + this.block.speed * elapsed) % 256;
			
			this.block.style.backgroundColor = "red";//`rgb(${this.block.r},${this.block.g},${this.block.b})`;
			}
		}
	}else{
		if(this.block.dir == "izq"){
		if(!arr_izq.includes(this.block.selec)){
			this.block.selec = (bloque_anterior.selec + 1) % 3072;
			var contenedor = document.getElementById("container");
			this.block.r = (this.block.r + this.block.speed * elapsed) % 256;
			this.block.g = (this.block.g + this.block.speed * elapsed) % 256;
			this.block.b = (this.block.b + this.block.speed * elapsed) % 256;
			this.block.style.backgroundColor = "red";//`rgb(${this.block.r},${this.block.g},${this.block.b})`;
		}
		}else{
			if(!arr_der.includes(this.block.selec)){
									
			this.block.selec = (bloque_anterior.selec + 1) % 3072;
			var contenedor = document.getElementById("container");
			this.block.r = (this.block.r + this.block.speed * elapsed) % 256;
			this.block.g = (this.block.g + this.block.speed * elapsed) % 256;
			this.block.b = (this.block.b + this.block.speed * elapsed) % 256;
			
			this.block.style.backgroundColor = "red";//`rgb(${this.block.r},${this.block.g},${this.block.b})`;
			}
		}
		
	}
  
	
/*	// this.block.style.backgroundColor = "white";
	if((this.block.x + this.block.speed * elapsed) <  contenedor.offsetHeight){
	this.block.x += this.block.speed * elapsed;
	}
	if((this.block.y + this.block.speed * elapsed) <  contenedor.offsetWidth){
    this.block.y += this.block.speed * elapsed;
	}
    this.block.style.left = this.block.x + "px";
    this.block.style.top = this.block.x + "px";
  }
	*/
	
	
	
  }

  render() {

  }
}
