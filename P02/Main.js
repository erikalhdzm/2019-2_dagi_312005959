import Interactive from "./Interactive.js";


window.addEventListener("load", function(evt) {    //Funcion para crear la reticula del contenedor
  let container = document.getElementById("container");
  let new_div;

  for (let i=0; i<3072; i++) {                          //Creación de cada div para la reticula
    new_div = document.createElement("div");
   // new_div.textContent = i;
   new_div.setAttribute("id", "div_"+i);
  
    container.appendChild(new_div);
  }

  
  // 32, 24
   let lastTime = Date.now();
  let current = 0;
  let elapsed = 0;
  let max_elapsed_wait = 30/1000;

  let interactive = new Interactive(document.getElementById("container"));

  (function gameLoop() {
    current = Date.now();
    elapsed = (current - lastTime) / 1000;

    if (elapsed > max_elapsed_wait) {
      elapsed = max_elapsed_wait;
    }

    interactive.processInput();
    interactive.update(elapsed);
    interactive.render();

    lastTime = current;

    window.requestAnimationFrame(gameLoop);
  })();
  });
  