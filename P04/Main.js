window.onload = function () {
	 var svgns = "http://www.w3.org/2000/svg";
     var svg = document.createElementNS(svgns,'svg');   //DeclaracionSvg
     var rectSize = 10;
     var score = 0;      // Variable puntaje
     var segSize = 10;
     var velocidad = 6	;     //Velocidad a la que va la viborita
     var sentido = 'right';      //sentidoeccion por defecto
     var snake = new Array(4);   //Viborita por defecto
     var inPlay = true;   //Nos dice si aun seguimos jugando
     var tablero = new Array(300);    //Espacio de canvas 
     	var snakeL = 2,
		rectArray = [];
	 
	 
	 for (i = 0; i < tablero.length; i++) {
       tablero[i] = new Array(300);    // Arreglo para el juego
     }
     for(i = 0; i < tablero.length; i++){
		 for(j = 0 ; j < tablero.length; j++){
			 tablero[i][j] = "A";
		 }
	 }
	 svg.setAttributeNS(null, 'width', 768);
     svg.setAttributeNS(null, 'height',576);
     svg.setAttributeNS(null, 'style', 'border: ' + rectSize + 'px solid #ccc;');
     
	 var body = document.getElementsByTagName('body')[0];
     body.appendChild(svg);
     tablero = crearSnake(tablero);    //Lamada a crear la serpiente
     tablero = crearManzana(tablero);       // Llamada a crear la comida 
	 
	 
     juegoSnake();

    //Funcion para crear una manzana 	
     function crearManzana(tablero) {

		  var manzanaX = Math.round(Math.random() * 30);
         var manzanaY = Math.round(Math.random() * 30);
         while (tablero[manzanaX][manzanaY] == 'S') {
            manzanaX = Math.round(Math.random() * 80);
            manzanaY = Math.round(Math.random() * 80);
         }
         tablero[manzanaX][manzanaY] = 'F';
		 manzana = [document.createElementNS(svgns, 'rect'), manzanaX, manzanaY];
		var thisManzana = manzana[0];
		
		thisManzana.setAttributeNS(null, 'x', manzanaX * rectSize);
        thisManzana.setAttributeNS(null, 'y', manzanaY * rectSize);
        thisManzana.setAttributeNS(null, 'height', rectSize);
        thisManzana.setAttributeNS(null, 'width', rectSize);
        thisManzana.setAttributeNS(null, 'class', 'manzana');
    	svg.appendChild(thisManzana);
         return tablero;
     }
	 
    //Funcion para pintar un punto en la coordenada x, y 
	function pintaPunto(x, y) {
		
		var rect = [document.createElementNS(svgns, 'rect'), x, y];
		var rectObj = rect[0];
		rectObj.setAttributeNS(null, 'x', x * rectSize);
        rectObj.setAttributeNS(null, 'y', y * rectSize);
        rectObj.setAttributeNS(null, 'height', rectSize);
        rectObj.setAttributeNS(null, 'width', rectSize);
        rectObj.setAttributeNS(null, 'class', 'snake');
		rectArray.push(rect);
    	svg.appendChild(rectObj);
    	if (rectArray.length > snakeL) {
    		svg.removeChild(rectArray[0][0]);
    		rectArray.shift();
    	}
	}
    //Funcion que crea la viborita, siempre comienza en la misma posicion	
     function crearSnake(tablero) {
         var snakeX = 31;    
         var snakeY = 25;
         while ((snakeX - snake.length) < 0) {
         snakeX = Math.round(Math.random() * 80);
         }
         for (i = 0; i < snake.length; i++) {
            snake[i] = {x: snakeX - i, y: snakeY};
            tablero[snakeX - i][snakeY] = "S";
         } 
         return tablero;
     }
	 
	 /*Funcion para mostrar al usuario su usuario y recordarle que le de refresh para vovler a jugar*/
     function finJuego() {
     alert("Juego terminao, el puntaje obtenido es : " + score + " Presione f5 para volver a intentar");
     }	
	 
	 
     /********* Comandos para cambio de direccion ****/
	 window.addEventListener('keydown', function (e) {
     if (e.keyCode == 37 && sentido != 'right') {
         sentido = 'left';
      } else if (e.keyCode == 38 && sentido != 'down') {
         sentido = 'up';
      } else if (e.keyCode == 39 && sentido != 'left') {
         sentido = 'right';
      } else if (e.keyCode == 40 && sentido != 'up') {
         sentido = 'down';
      }
     });
	 
	 
	 //Funcion Juego Principal
     function juegoSnake() {     
   //  context.clearRect(0, 0, svg.width, svg.height);
     for (i = snake.length - 1; i >= 0; i--) {
        if (i == 0) {
            switch (sentido) {
                case 'right':
                   if (sentido != 'left') {
                       snake[0] = { x: snake[0].x + 1, y: snake[0].y }
                break;
                }
                case 'left':
                   if (sentido != 'right') {
                       snake[0] = { x: snake[0].x - 1, y: snake[0].y }
                break;
                }
                case 'up':
                     if (sentido != 'down') {
                        snake[0] = { x: snake[0].x, y: snake[0].y - 1 }
                break;
                }
                case 'down':
                    if (sentido != 'up') {
                       snake[0] = { x: snake[0].x, y: snake[0].y + 1 }
                break;
                }
            }
			
			/*******Verificación de choque con las esquinas *******/
            if(snake[0].x < 0 || snake[0].x >=768 ||snake[0].y <0 || snake[0].y >=768) {
               finJuego();
               return;
            }
			
			/*******Verificación de cuando se come una manzana ******/
			
            if(tablero[snake[0].x][snake[0].y] == 'F' ) {
                score++;     //Sube el score
				svg.removeChild(manzana[0]);
                tablero=crearManzana(tablero);   //Se vuelve a crear una manzana
            }else if (tablero[snake[0].x][snake[0].y] == 'S') {
                finJuego();
                return;
            }
            tablero[snake[0].x][snake[0].y] = 'S';
            } else {
                if(i==(snake.length-1)){
                    tablero[snake[i].x][snake[i].y]=null;
                }
                snake[i]={
                x: snake[i-1].x,
                y: snake[i-1].y
                };
				pintaPunto(snake[i].x, snake[i].y);
                tablero[snake[i].x][snake[i].y] = 'S';
           }
     }
	 
     
        if(inPlay){
           setTimeout(juegoSnake, 400-(velocidad*50));
        }
     }
	 
};