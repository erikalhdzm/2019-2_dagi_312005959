var canvas = document.getElementById("game");
var context = canvas.getContext("2d");   //Declaracion del canvas

var spaceship = {   //Declaracion de inicio de la nave 
    color: "white",
    width: 8,
    height: 25,
    position:{
        x: 20,
        y: 20
    },
    velocity:{
        x: 0,
        y: 0
    },
	thrust : -0.007,
    angle: Math.PI / 2,
    engineOn: false,
    rotatingLeft: false,
    rotatingRight: false,
    crashed: false
}

function drawSpaceship() { // Función que dibuja a la nave espacial
    context.save();
    context.beginPath();
    context.translate(spaceship.position.x, spaceship.position.y);
    context.rotate(spaceship.angle);
    context.rect(spaceship.width * -0.5, spaceship.height * -0.5, spaceship.width, spaceship.height);
    context.fillStyle = spaceship.color;
    context.fill();
    context.closePath();


    if(spaceship.engineOn){  //Dibujar la flama de la nave
        context.beginPath();
        context.moveTo(spaceship.width * -0.5, spaceship.height * 0.5);
        context.lineTo(spaceship.width * 0.5, spaceship.height * 0.5);
        context.lineTo(0, spaceship.height * 0.5 + Math.random() * 10);
        context.lineTo(spaceship.width * -0.5, spaceship.height * 0.5);
        context.closePath();
        context.fillStyle = "orange";
        context.fill();
    }
    context.restore();
}

posicion_triangulos_X = [];
posicion_triangulos_Y = [];

definidos = false;

function drawTerrain(figs){   // Función que dibuja los obstáculos

	posibles_figs = [0,1] // Triángulos y Rectángulos
	pos_anterior = 2;
	
	context.fillStyle = "green";
	for(var i = 1; i <= 5 ; i++){
	
	if(figs[i-1]% 2 == 1){
		context.beginPath();
		context.moveTo(i*150, i*400);
		context.lineTo(i*200, i*100);
		context.lineTo(i*200, i*350);
		if(!definidos){
			posicion_triangulos_X.push(i*150);
			posicion_triangulos_X.push(i*200);
			posicion_triangulos_X.push(i*200);
			posicion_triangulos_Y.push(i*400);
			posicion_triangulos_Y.push(i*100);
			posicion_triangulos_Y.push(i*350);
		}
		context.closePath();
		context.fill();
	}
		context.rect((i*100)-100,450,400,50);
		context.fill();
	}
	
	definidos = true;
    context.fillStyle = "red";
}


var gravity =  0.0007;
figs = [1,2,3,4,5];
	
function pntInTriangle(px, py,  x1, y1,x2, y2, x3,  y3) { // Función que revisa si un punto está dentro de un triángulo para la colision

    var o1 = getOrientationResult(x1, y1, x2, y2, px, py);
    var o2 = getOrientationResult(x2, y2, x3, y3, px, py);
    var o3 = getOrientationResult(x3, y3, x1, y1, px, py);

    return (o1 == o2) && (o2 == o3);
}
// Función auxiliar para revisar si un punto está dentro de un triángulo, revisa las coordenadas baricéntricas
function getOrientationResult( x1,  y1,  x2,  y2,  px,  py) {
    var orientation = ((x2 - x1) * (py - y1)) - ((px - x1) * (y2 - y1));
    if (orientation > 0) {
        return 1;
    }
    else if (orientation < 0) {
        return -1;
    }
    else {
        return 0;
    }
}

function enTriangulo(x, y){ // Función que revisa si un punto está en algún triángulo para la colision
	
	for(i=0; i< 9; i+=3){
	x1 = posicion_triangulos_X[i];
	y1 = posicion_triangulos_Y[i];
	x2 = posicion_triangulos_X[i+1];
	y2 = posicion_triangulos_Y[i+1];
	x3 = posicion_triangulos_X[i+2];
	y3 = posicion_triangulos_Y[i+2];
	
	if(pntInTriangle(x,y,x1,y1,x2,y2,x3,y3)){
		return true;
		}
    }
	
	return false;
}
// Función que revisa si un punto está en todo el rectángulo que sirve de suelo
function enRectangulo(x, y){
	  if (x > 100  && x < 800 && 
        y > 450  && y < 500) 
        return true; 
	
    return false; 
}
primera_vez = true;
// Función actualiza la nave, calculando velocidad y orientación de acorde a las teclas que va presionando el usuario
function updateSpaceship(){
	
	if(enTriangulo(spaceship.position.x, spaceship.position.y) && !primera_vez){ // Se pierde
		alert("¡Has Perdido!");
		
	}
	else if(enRectangulo(spaceship.position.x, spaceship.position.y)){ // Se gana
		alert("¡Has Ganado!");
		
	}else{
		primera_vez = false;
		spaceship.position.x += spaceship.velocity.x;
    spaceship.position.y += spaceship.velocity.y;
	 if(spaceship.rotatingRight)
    {
        spaceship.angle += Math.PI / 180;
    }
    else if(spaceship.rotatingLeft)
    {
        spaceship.angle -= Math.PI / 180;
    }

    if(spaceship.engineOn)
    {
        
		spaceship.velocity.x += spaceship.thrust * Math.sin(-spaceship.angle);
        spaceship.velocity.y += spaceship.thrust * Math.cos(spaceship.angle);
    }
	
	  spaceship.velocity.y += gravity;
	}
   
}

function draw(){ // Función que dibuja todos los elementos en el canvas
    
    context.clearRect(0, 0, canvas.width, canvas.height);
    updateSpaceship();
    drawStars();
    drawSpaceship();
	drawTerrain(figs);
    requestAnimationFrame(draw);
}

function drawStars() { // Función que dibuja el fondo
	var stars = [];
	for (var i = 0; i < 500; i++) {
  stars[i] = {
    x: Math.random() * canvas.width,
    y: Math.random() * canvas.height,
    radius: Math.sqrt(Math.random() * 2),
    alpha: 1.0,
    decreasing: true,
    dRatio: Math.random()*0.05,
  };
}
  context.save();
  context.fillStyle = "#111"
  context.fillRect(0, 0, canvas.width, canvas.height);
  for (var i = 0; i < stars.length; i++) {
    var star = stars[i];
    context.beginPath();
    context.arc(star.x, star.y, star.radius, 0, 2*Math.PI);
    context.closePath();
    context.fillStyle = "rgba(255, 255, 255, " + star.alpha + ")";
    if (star.decreasing == true)
    {
      star.alpha -= star.dRatio;
      if (star.alpha < 0.1)
      { star.decreasing = false; }
    }
    else
    {
      star.alpha += star.dRatio;
      if (star.alpha > 0.95)
      { star.decreasing = true; }
    }
    context.fill();
  }
  context.restore();
}

/***************************************A partir de aqui es para las teclas *****************************************/
// Función que revisa cuando se deja de presionar una tecla
function keyLetGo(event){
    
    switch(event.keyCode)
    {
        case 37:
            // Left Arrow key
            spaceship.rotatingLeft = false;
            break;
        case 39:
            // Right Arrow key
            spaceship.rotatingRight = false;
            break;
        case 38:
            // Up Arrow key
            spaceship.engineOn = false;
            break;
    }
}

document.addEventListener('keyup', keyLetGo);

// Función que revisa si una tecla es presionada
function keyPressed(event){
    
    switch(event.keyCode)
    {
        case 37:
            // Left Arrow key
            spaceship.rotatingLeft = true;
            break;
        case 39:
            // Right Arrow key
            spaceship.rotatingRight = true;
            break;
        case 38:
            // Up Arrow key
            spaceship.engineOn = true;
            break;
    }
}
// Función que revisa si se mueve hacia arriba
function moveup() {
 spaceship.engineOn = true;
 
setTimeout(function(){
    nomoveup();
}, 500);
}
// Función que indica no más movimiento hacia arriba
function nomoveup(){
spaceship.engineOn = false;	
}
// Función que revisa si se mueve hacia la izquierda
function moveleft() {
   spaceship.rotatingLeft = true;
  setTimeout(function(){
    nomoveleft();
}, 500);
}
// Función que indica no más movimiento hacia la izquierda
function nomoveleft(){
	spaceship.rotatingLeft = false;
}
// Función que revisa si se mueve hacia la derecha
function moveright() {
  spaceship.rotatingRight = true;
  setTimeout(function(){
    nomoveright();
}, 500);
}
// Función que indica no más movimiento hacia la derecha
function nomoveright(){
	spaceship.rotatingRight = false;
}
document.addEventListener('keydown', keyPressed);
draw();