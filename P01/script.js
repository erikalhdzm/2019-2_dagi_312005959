var turno = 0; // Par -> Tache, Impar -> Círculo
var hayGanador = false;
var pos_taches = [];
var pos_circulos = [];
function llena(id){
	if(!hayGanador){
		// Se verifica no haya sido activado previamente el recuadro		
		if(turno % 2 == 0){
			if(pos_taches.indexOf(id) == -1 && pos_circulos.indexOf(id) == -1){
				 document.getElementById(id).hidden = true;
			     document.getElementById("tache"+id).hidden = false;
				  document.getElementById("nombre_h").innerHTML = "Círculo";
				pos_taches.push(id);
				hayGanador = revisaGanador(pos_taches, id);
				if(hayGanador){
				alert("Felicidades, tache ha ganado");	
				}
			}else{
				alert("La casilla ya ha sido jugada");
			}
		}else{
			if(pos_taches.indexOf(id) == -1 && pos_circulos.indexOf(id) == -1){
				document.getElementById(id).hidden = true;
				 document.getElementById("circulo"+id).hidden = false;
				 document.getElementById("nombre_h").innerHTML = "Tache";
				pos_circulos.push(id);
				hayGanador = revisaGanador(pos_circulos, id);
				if(hayGanador){
				alert("Felicidades, círculo ha ganado");	
				}
			}else{
				alert("La casilla ya ha sido jugada");
			}
		}
		
		turno ++;
	}
	else{
		var ganador = (turno%2==0)?"Círculo":"Gato";
		alert("El juego ha terminado, ganó " + ganador);
	}
}

	
function revisaGanador(arreglo, idx){
	switch(idx){
		case 1:
			if(arreglo.indexOf(2) != -1 && arreglo.indexOf(3) != -1){
				return true;
			}
			else if(arreglo.indexOf(4) != -1 && arreglo.indexOf(7) != -1){
				return true;
			}else if(arreglo.indexOf(9) != -1 && arreglo.indexOf(5) != -1){
				return true;
			}
			return false;
		break;
		case 2:
			if(arreglo.indexOf(1) != -1 && arreglo.indexOf(3) != -1){
				return true;
			}else if(arreglo.indexOf(5) != -1 && arreglo.indexOf(8) != -1){
				return true;
			}
			return false;
		break;
		case 3:
			if(arreglo.indexOf(2) != -1 && arreglo.indexOf(1) != -1){
				return true;
			}else if(arreglo.indexOf(5) != -1 && arreglo.indexOf(7) != -1){
				return true;
			}else if(arreglo.indexOf(9) != -1 && arreglo.indexOf(6) != -1){
				return true;
			}
			return false;
		break;
		case 4:
			if(arreglo.indexOf(1) != -1 && arreglo.indexOf(7) != -1){
				return true;
			}else if(arreglo.indexOf(5) != -1 && arreglo.indexOf(6) != -1){
				return true;
			}
			return false;
		break;
		case 5:
			if(arreglo.indexOf(2) != -1 && arreglo.indexOf(8) != -1){
				return true;
			}else if(arreglo.indexOf(1) != -1 && arreglo.indexOf(9) != -1){
				return true;
			}else if(arreglo.indexOf(4) != -1 && arreglo.indexOf(6) != -1){
				return true;
			}else if(arreglo.indexOf(7) != -1 && arreglo.indexOf(3) != -1){
				return true;
			}
			return false;
		break;
		case 6:
			if(arreglo.indexOf(3) != -1 && arreglo.indexOf(9) != -1){
				return true;
			}else if(arreglo.indexOf(4) != -1 && arreglo.indexOf(5) != -1){
				return true;
			}
			return false;
		break;
		case 7:
			if(arreglo.indexOf(4) != -1 && arreglo.indexOf(1) != -1){
				return true;
			}else if(arreglo.indexOf(8) != -1 && arreglo.indexOf(9) != -1){
				return true;
			}
			return false;
		break;
		case 8:
			if(arreglo.indexOf(7) != -1 && arreglo.indexOf(9) != -1){
				return true;
			}else if(arreglo.indexOf(5) != -1 && arreglo.indexOf(2) != -1){
				return true;
			}
			return false;
		break;
		case 9:
			if(arreglo.indexOf(3) != -1 && arreglo.indexOf(6) != -1){
				return true;
			}else if(arreglo.indexOf(7) != -1 && arreglo.indexOf(8) != -1){
				return true;
			}
			return false;
		break;
	}
}